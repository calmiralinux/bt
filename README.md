# bt

`bt` - программа для получения информации о проценте заряда аккумуляторной
батареи ноутбука. В планах добавление нового функционала.

## Build and install

```bash
% cargo build --release
% sudo mv -v target/release/bt /usr/bin/bt
```

## License

GNU GPLv3
