/*
 * Copyright (C) 2022, 2023, Michail Krasnov <linuxoid85@gmail.com>
 * For Calmira 2.0 GNU/Linux-libre
 */

use std::process::exit;
use std::path::Path;
use std::io::Result;
use std::fs;

fn get_file(path: &Path) -> Result<String> {
    Ok(fs::read_to_string(path)?)
}

fn main() {
    let bat_file = Path::new("/sys/class/power_supply/BAT1/capacity");

    if !bat_file.exists() || !bat_file.is_file() {
        eprintln!("File with information about battery not found!");
        exit(1);
    }

    match get_file(&bat_file) {
        Ok(c) => println!("{}", c.trim()),
        Err(why) => eprintln!("{why}"),
    }
}
